package ideOnline.util;

import java.io.FileInputStream;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import ideOnline.interfaces.ConexaoSFTP;

/**
 * Classe que se conecta com um servidor remoto
 * 
 * @author Isa�asSantana
 * @since 03/04/2016
 * @version 0.1
 */
public class ClienteSFTP implements ConexaoSFTP
{
	/* A se��o de comunica��o */
	private Session session;
	/* O diret�rio remoto onde ir� salvar os c�digos fontes */
	private final String SFTP_WORKING_DIR = "/home/isaias/";

	@Override
	public boolean criarConexao(String user, String password, String host, int port) {
		JSch jsch = new JSch();
		try
		{
			session = jsch.getSession(user, host, port);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();
			return true;
		}
		catch(JSchException e)
		{
			return false;
		}
	}

	@Override
	public boolean fecharConexao() {
		
		return false;
	}

	@Override
	public void uploadFile(FileInputStream file,String fileName)
	{
		ChannelSftp channelSftp = null;
		try 
		{
			Channel channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;
			try
			{
				channelSftp.cd(SFTP_WORKING_DIR);
				channelSftp.put(file, fileName);
			}
			catch (SftpException e)
			{
			
				e.printStackTrace();
			}
			
			channelSftp.disconnect();
			channel.disconnect();
			
		}
		catch (JSchException e)
		{
				e.printStackTrace();
		}
	}

}
