package ideOnline.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import ideOnline.interfaces.ConexaoSSH;

/**
 * 
 * @author Isa�asSantana
 * @since 10/03/2016
 * @version 0.2
 */
public class ClienteSSH implements ConexaoSSH{
	
	private Channel channel;
	private Session session;
	private JSch jsch;
	private InputStream readChannel;
	private OutputStream writeToChannel;
	private String nomeHost;
	private static final String BARRA_N = "\n";
	
	public String getNomeHost()
	{
		return  nomeHost != null ? nomeHost : "Terminal";
	}
	
	@Override
	public boolean criarConexao(String user, String password,String host, int port) 
	{
		jsch = new JSch();
		try 
		{
			session = jsch.getSession(user,host, port);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);
			session.connect();
			
			//Cria o canal de dados.
			channel = session.openChannel("shell");
			try 
			{
				readChannel = channel.getInputStream();
				writeToChannel = channel.getOutputStream();
				((ChannelShell)channel).setPtyType("vt102");
		        channel.connect();
		        nomeHost = getNomeHostRemoto();
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		catch (JSchException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean fecharConexao()
	{
		try {
			readChannel.close();
			writeToChannel.close();
			session.disconnect();
			channel.disconnect();
			return true;
		} catch (IOException e) {

			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 
	 * @return retorna o nome do host remoto;
	 */
	private String getNomeHostRemoto()
	{
		byte[] resp = new byte[256];
		String saida = null;
		try {
			while(readChannel.read(resp, 0,resp.length) > 0)
			{
				saida = new String(resp);
				if(saida.contains("~$")) break;
			}
			return saida.substring(0, saida.lastIndexOf("~$"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Processa a string de sa�da do terminal. Retornando apenas o resultado esperado de um comando executado. 
	 * @param saidaTerminal
	 * @return a sa�da esperada.
	 */
	private  String processaStringTerminal(String saidaTerminal)
	{
		if(saidaTerminal.contains(nomeHost)) return saidaTerminal.substring(0, saidaTerminal.lastIndexOf(nomeHost));
		
		return saidaTerminal;
	}
	
	
	/**
	 * Executa o comando passado.
	 * @param command o comando a ser executado.
	 * @return o resultado do processamento do comando.
	 * @throws IOException
	 */
	public String cmd(String command) throws IOException 
	{
		writeToChannel.write((command+BARRA_N).getBytes());
		writeToChannel.flush();
		StringBuffer buffer = new StringBuffer();
		
		byte resposta[] = new byte[256];
	    String out = null;
	    int tamString = 0;
	    int contador = 0;
	    
	    while(true)
	    {
	    	if(out == null) ; else tamString = out.length();
	    	if(out!=null && out.length() == tamString)
	    	{
	    		contador++;
	    		if (contador == 2)break; // Se for 2, poderia ser mais. Indica que no canal n�o h� nada para ler. E com isso sai do la�o infinito
	    	}
	    	while(readChannel.available() > 0)
	    	{
	    		int i = readChannel.read(resposta,0,256);
	    		if(i < 0) break;
	    		out = new String(resposta,0,i,"UTF-8");
	    		buffer.append(out+BARRA_N);
	    	}
	    	if(channel.isClosed())
	    	{
	    		if(readChannel.available() > 0)
	    		{
	                int i = readChannel.read(resposta, 0, 256);
	                out = new String(resposta,0,i,"UTF-8");
	                buffer.append(out+BARRA_N);
	            }
	    		if (channel.getExitStatus() == 0)
	    		{ //Colocar um di�logo depois.
	                System.out.println("</br></br>");
	                System.out.println("Command has been posted to the server.");
	            }
	            break;
	    	}
	    	try
	    	{
	            Thread.sleep(600);
	        } 
	    	catch (Exception ee) {}
	     }
	    
	      String saida = processaStringTerminal(buffer.toString());
	      return saida;
	     
	}
	
}
