package ideOnline.interfaces;

public interface ConexaoSSH 
{
	
	boolean criarConexao(String user,String password, String host, int port);
	boolean fecharConexao();
	
}
