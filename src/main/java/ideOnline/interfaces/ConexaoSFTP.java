package ideOnline.interfaces;


import java.io.FileInputStream;

/**
 * Estabelece m�todos para criar uma conex�o e enviar arquivos para o servidor remoto.
 * @author Isa�asSantana
 *
 */
public interface ConexaoSFTP extends ConexaoSSH
{
	/**
	 * Envia arquivos para o host remoto.
	 * @param file o arquivo a ser transferido.
	 * @param fileName o nome do arquivo com sua exten��o, exemplo "ABC.java"
	 */
	void uploadFile(FileInputStream file,String fileName);
}
