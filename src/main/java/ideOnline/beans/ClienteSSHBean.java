package ideOnline.beans;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.jcraft.jsch.JSchException;

import ideOnline.util.ClienteSFTP;
import ideOnline.util.ClienteSSH;

/**
 * Classe controladora do terminal.
 * @author Isa�asSantana
 * @since 10/03/2016
 */
@ManagedBean
@ViewScoped
public class ClienteSSHBean 
{
	private ClienteSSH clienteSSH;
	private ClienteSFTP clienteSFTP;
	private String codigo;
	
	public ClienteSSHBean()
	{
		clienteSSH = new ClienteSSH();
		clienteSSH.criarConexao("isaias", "buceta", "192.168.0.103", 22);
		
		clienteSFTP = new ClienteSFTP();
		clienteSFTP.criarConexao("isaias", "buceta", "192.168.0.103", 22);
	}
	
	/**
	 * Envia o arquivo fonte via SFTP. Primeiro salva o arquivo no disco local, depois envia via SFTP para o host remoto.
	 * @return null para permanecer na mesma p�gina.
	 * @throws FileNotFoundException
	 */
	public String saveFile() throws FileNotFoundException
	{	
		try
		{
			codigo =  FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("code");
			if(!codigo.equals(null))
			{
				
				Path path = Paths.get("E:/Codigos fontes/Teste.java"); //Pasta onde salvo os fontes.
				Files.write(path, codigo.getBytes("UTF-8"));
				clienteSFTP.uploadFile(new FileInputStream("E://Codigos fontes//teste.java"), "Teste.java"); 
			}
		}
		catch (IOException e)
		{
		
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Obt�m o nome do servidor remoto.
	 * @return O nome do servidor remoto, ou "terminal" como um nome padr�o caso n�o consiga obter o nome do host remoto.
	 */
	public String getNomeHost()
	{
	if(!clienteSSH.getNomeHost().equals(null)) return clienteSSH.getNomeHost()+"~$";
		return "Terminal";
	}
	
	/**
	 * Ouve os comandos digitados no emulador de terminal.
	 * @param command o comando digitado no emulador.
	 * @param params par�metros adicionais.
	 * @return retorna o uma string como resultado do processamento do comando.
	 * @throws IOException
	 * @throws JSchException
	 */
	 public  String handleCommand(String command, String[] params) throws IOException, JSchException
	 { 
		 if(params.length > 0)
		 {
			 StringBuffer buffer = new StringBuffer();
			 buffer.append(command+" ");
			 for(String parametro:params) buffer.append(parametro+" ");
			 
			 String respostaCmd = clienteSSH.cmd(buffer.toString());
			 return  respostaCmd;
		 }
		
		 String respostaCmd = clienteSSH.cmd(command);
		 
		 return respostaCmd;
	 }	
	 
	 public void setCodigo(String codigo)
	 {
		 this.codigo = codigo;
	 }
	 
	 public String getCodigo()
	 {
		 return codigo;
	 }
}
